const express = require('express');
const router = express.Router();


//Route to get all the tasks
//This route expects to receive a GET request at the URL "/tasks"
//The whole URL is at "http://localhost:3001/tasks" this is because of the "/tasks"
router.get('/', (req, res)=>{
	taskController.getAllTasks();
})

//Router to create a new task
//This route expects to receive a POST request at the URL "/tasks"
//The whole URL is at "http://localhost:3001/tasks"
router.post('/', (req, res)=>{
	 taskController.addNewTask();
})

//Use module.exports to export the router object so we can use it in index.js
module.exports = router;