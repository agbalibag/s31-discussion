//setup dependencies
const express = require('express');
const mongoose = require('mongoose');
//This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require('./routes/taskRoute');
//Server Setup
const app = express();
const port = 3001;
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Database Connection
//Connecting to MongoDB Atlas
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.upmgf.mongodb.net/s31?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

// Server listening
app.listen(port, ()=>console.log(`Listening to port ${port}`));